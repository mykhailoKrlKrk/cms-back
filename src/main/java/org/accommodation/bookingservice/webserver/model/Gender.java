package org.accommodation.bookingservice.webserver.model;

public enum Gender {
    M,
    F
}
