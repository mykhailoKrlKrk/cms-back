package org.accommodation.bookingservice.webserver.model;

public enum Status {
    isActive,
    noActive
}
