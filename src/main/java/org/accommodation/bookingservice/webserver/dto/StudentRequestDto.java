package org.accommodation.bookingservice.webserver.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDate;
import lombok.Data;
import org.accommodation.bookingservice.webserver.model.Gender;
import org.accommodation.bookingservice.webserver.model.Status;
import org.accommodation.bookingservice.webserver.validation.Formatter;

@Data
public class StudentRequestDto {
    private String group;
    @Formatter
    private String firstName;
    @Formatter
    private String lastName;
    private Gender gender;
    private Status status;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthday;
}
