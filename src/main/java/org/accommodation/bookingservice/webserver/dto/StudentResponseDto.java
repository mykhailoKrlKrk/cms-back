package org.accommodation.bookingservice.webserver.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDate;
import lombok.Data;
import org.accommodation.bookingservice.webserver.model.Gender;
import org.accommodation.bookingservice.webserver.model.Status;
import org.accommodation.bookingservice.webserver.validation.Formatter;
import org.springframework.format.annotation.DateTimeFormat;

@Data
public class StudentResponseDto {
    private Long id;
    private String group;
    private String firstName;
    private String lastName;
    private Gender gender;
    private Status status;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthday;
}
