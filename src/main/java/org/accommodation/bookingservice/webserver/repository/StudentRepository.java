package org.accommodation.bookingservice.webserver.repository;

import org.accommodation.bookingservice.webserver.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {
}
