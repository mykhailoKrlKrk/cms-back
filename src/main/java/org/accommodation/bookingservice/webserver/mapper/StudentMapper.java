package org.accommodation.bookingservice.webserver.mapper;

import org.accommodation.bookingservice.webserver.config.MapperConfig;
import org.accommodation.bookingservice.webserver.dto.StudentRequestDto;
import org.accommodation.bookingservice.webserver.dto.StudentResponseDto;
import org.accommodation.bookingservice.webserver.model.Student;
import org.mapstruct.Mapper;

@Mapper(config = MapperConfig.class)
public interface StudentMapper {

    StudentResponseDto toDto(Student student);

    Student toModel(StudentRequestDto requestDto);
}
