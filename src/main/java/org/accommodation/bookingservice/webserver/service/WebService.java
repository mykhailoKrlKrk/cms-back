package org.accommodation.bookingservice.webserver.service;

import java.util.List;
import org.accommodation.bookingservice.webserver.dto.StudentRequestDto;
import org.accommodation.bookingservice.webserver.dto.StudentResponseDto;

public interface WebService {
    List<StudentResponseDto> getAll();
    StudentResponseDto createStudent(StudentRequestDto requestDto);
    StudentResponseDto editStudent(Long id, StudentRequestDto requestDto);

    boolean validate(StudentRequestDto requestDto);

    void deleteStudent(Long id);
}
